﻿#include <iostream>

void FindOddNumbers(int Limit, bool IsOdd)
{
    for (int i = 0; i <= Limit; i++)
    {
        if (IsOdd)
        {
            std::cout << i << std::endl;
            i++;
        }
        else
        {
            ++i;
            std::cout << i << std::endl;
        }
            
    }
    std::cout << std::endl;
}

int main()
{
    const int N = 15;
    
    FindOddNumbers(N, true);
    FindOddNumbers(N, false);

}


